# AIGRO_SIM

Simulation of robot models in different environments with Gazebo simulator an ROS2

## Getting started

0. Install ROS2, Gazebo 
1. Clone this repo
2. Install Turtlebot3 files (script `scripts/install_turtlebt.py` will help you with that)
3. Build workspace with `colcon build --symlink-install`
4. Source workspace 
    * add `path/to/workspace/install/setup.bash` to `~/.bashrc`
    * `source ~/.bashrc`
5. Test it!  `ros2 launch aigro_sim trees_world.launch.py `


## How we work on the code

Please follow the following guideline:

* add new *functionality* on **new branches** with a *descriptive name*
* push the branch and send a merge request to repository maintainer

**Note** : bug fixes and small (non-functional) changes may be done directly on the master branch without the merge requiest overhead. ...just make sure that you don't introduce more problems than you solve ;-)


