#!/usr/bin/env python

#
import sys
import os
from pathlib import Path
import click

# first, get location of the workspace
try:    
    WS_DIR = Path(os.environ['TURTLEBOT3_WS'])
except KeyError:
    print('please define target directory first `export TURTLEBOT3_WS=/path/to/turtlebot3_ws')
    sys.exit()



cwd = os.getcwd()

@click.command()
def clone():
    """ 
    clone required directories into new workspace named `turtlebot_ws`
    """

    src_dir = Path(WS_DIR) / 'src'
    if not src_dir.exists():
        if click.confirm(f'{src_dir.as_posix()} does not exist. Create? \n?', default = True):
            src_dir.mkdir(parents=True)
        else:
            print('Aborting')
            return

    os.chdir(src_dir)
    os.system('git clone -b foxy-devel https://github.com/ROBOTIS-GIT/turtlebot3_msgs.git')
    os.system('git clone -b foxy-devel https://github.com/ROBOTIS-GIT/turtlebot3.git')
    os.system('git clone -b foxy-devel https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git')


@click.command()
def modify_bashrc():
    """ add section to ~/.bashrc """
    if not click.confirm('Add exports to .bashrc?', default = False):
        return
    
    with open(Path.home() / '.bashrc', 'a') as fid:
        fid.write(f'# ----start {__file__}\n')
        model = click.prompt('enter turtlebot model: ', default = 'burger')
        fid.write(f'export TURTLEBOT3_MODEL={model}\n')
        fid.write('source '+ (WS_DIR / 'install/setup.bash\n').as_posix())
        fid.write(f'# ----end {__file__}\n')

    print('Now run `source ~/.bashrc`')

@click.command()
def build():
    os.chdir(WS_DIR)
    os.system('colcon build --symlink-install')


@click.group()
def cli():
    pass


cli.add_command(clone)
cli.add_command(build)
cli.add_command(modify_bashrc)

if __name__ == "__main__":
    cli()