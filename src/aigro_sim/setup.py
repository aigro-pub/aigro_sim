from setuptools import setup
from os.path import join
from glob import glob
import os
from pathlib import Path

package_name = 'aigro_sim'

#%%
def add_data_files(directory):
    """ packages all files in a directory """
    src = glob(directory + '/*')
    dest = join('share',package_name,directory)
    return (dest,src)
    


#%%
setup(
    name=package_name,
    version='0.0.1',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        add_data_files('launch'),
        add_data_files('models/tree_trunk'),
        add_data_files('worlds'),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Jev Kuznetsov',
    maintainer_email='jev@aigro.nl',
    description='Robot simulations',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
        ],
    },
)
